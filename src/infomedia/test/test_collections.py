import utils
import unittest
from pprint import pprint
import logging

from infomedia.vdict   import *
from infomedia.hash2cfg.defines      import DefineSet

# def test_vdict_01():
#     d = vdict({'A':1,'b':2})
#     d['pippo']=99
#     d['pLuTo']=101
#     assert d['B']==2
#     assert d['PIPPO']==99
#     assert d['PLUTO']==101
#     assert d.has_key('pippo')
#     assert 'pippo' in d
#     pprint(d)
# 
# 

d1= {
    'SUB' : {
        'key': 'X',
        'SUB': {
            'key': 'Y'
            }
    },
    'key': '1',
    'cond1|$A': 'A',
    'cond1|not $A': 'B',    
    'bool00': 'oFf',
    'bool01': '0',
    'bool02': 'no',
    'bool03': 'false',
    'bool10': 'oN',
    'bool11': '1',
    'bool12': 'yes',
    'bool13': 'true',
}


class ExpansionTest(unittest.TestCase):
    def test_expand_var01(self):
#       import pdb
#        pdb.set_trace()
        defines = DefineSet({ 'A': True, 'B_A': '1' })
        e = expansion('$A $B_A ${B_A}',defines)
        self.assertEqual(u'True 1 1',e)

class DatastructTest(unittest.TestCase):
    def test_get_list(self):
        """test functions: get_list, get_numlist, get_intlist"""

        l = get_list(None)
        self.assertIsNone(l)
        
        l = get_list('UNO,DUE,TRE')
        self.assertEqual(l[2],'TRE')

        self.assertIsNone(get_numlist(None))

        l = get_numlist('1,2,3')
        self.assertEqual(l[2],3)

        l = get_numlist('1!2!3',sep='!')
        self.assertEqual(l[2],3)

        self.assertRaises(ValueError,get_numlist,'1,2,3.1')

        l = get_intlist('1,2,3')
        self.assertEqual(l[2],3)

    def test_boolean(self):
        self.assertTrue(boolean('trUe'))
        self.assertTrue(boolean('yes'))
        self.assertTrue(boolean('oN'))
        self.assertTrue(boolean('1'))
        self.assertTrue(boolean(True))
        self.assertTrue(boolean(75))
        self.assertFalse(boolean('fAlse'))
        self.assertFalse(boolean('nO'))
        self.assertFalse(boolean('oFf'))
        self.assertFalse(boolean('0'))
        self.assertFalse(boolean(False))
        self.assertFalse(boolean(0))
        self.assertFalse(boolean('pippo'))
        self.assertFalse(boolean('not $A'))
        self.assertFalse(boolean({'A':0}))

    def test_needed(self):
        self.assertTrue(needed(d1,None))
        self.assertTrue(needed(d1,'bool01','bool12'))
        self.assertRaises(KeyError,needed,d1,'bool01','bool12','nokey')

    def test_accepted(self):
        self.assertRaises(ValueError,accepted, d1,None)
        self.assertTrue((d1,'bool01','bool12'))

    def test_keycond(self):
        self.assertEqual(keycond('TEST'),('TEST',None))
        self.assertEqual(keycond('TEST|GOOD'),('TEST','GOOD'))


    def test_grep00(self):
        self.assertListEqual(grep('00$',d1),['bool00'])
        r = grep('0.$',d1)
        self.assertEqual(len(r),4)

        # on original dict conditions are included in key
        # so next grep finds only bool01 and bool11 not
        # cond1
        r = grep('1$',d1)        
        self.assertEqual(len(r),2)

        # in vdict instead cond1 is a key so here
        # grep finds bool01, bool11 *and* cond1
        d = vdict(d1)
        r = grep('1$',d)        
        self.assertEqual(len(r),3)

        r = grep('1$',d,limit=1)
        self.assertEqual(len(r),1)

        # order is alphabetic in dicts, respected in lists
        r = grep('1$',sorted(d.keys()),limit=1)
        self.assertListEqual(r,['bool01']) 
        r = grep('^b',sorted(d),limit=1)
        self.assertListEqual(r,['bool00']) 

    def test_select(self):
        pass

    def test_compose01(self):
        pass

    def test_extend_booleans(self):
        pass

    def save_dict(self):
        pass


class VDictTest(unittest.TestCase):

    def test_00init1(self):
        d = vdict()
        self.assertDictEqual( d, {} )

    def test_00init2(self):
        d = vdict(d1)
        r =   {'SUB': {'SUB': {'key': 'Y'}, 'key': 'X'},
                '__cond1_COND': ['$A', 'not $A'],
                'bool00': 'oFf',
                'bool01': '0',
                'bool02': 'no',
                'bool03': 'false',
                'bool10': 'oN',
                'bool11': '1',
                'bool12': 'yes',
                'bool13': 'true',
                'cond1': ['A', 'B'],
                'key': '1'}
        self.assertDictEqual( d, r )


    def test_01haskey1(self):
        d = vdict(d1)

        self.assertTrue(d.has_key('key'))
        self.assertFalse(d.has_key('notkey'))


        # no key if all conditions are false
        self.assertFalse(d.has_key('cond1'))

        defines = DefineSet({ 'A': True })
        self.assertFalse('cond1' in d)
        self.assertTrue(d.has_key('cond1',defines=defines))
        self.assertFalse('cond1' in d)

        defines = DefineSet({ 'A': False })
        self.assertFalse('cond1' in d)
        self.assertTrue(d.has_key('cond1',defines=defines))
        self.assertFalse('cond1' in d)

    def test_02bool(self):
        d = vdict(d1)

        for i in range(2):
            for j in range(4):
                key = "bool%1d%1d" % (i,j)
                val = d.get_bool(key)
                # print key,val,i,d.xget(key)
                self.assertEqual(val,bool(i))

    def test_01xget1(self):
        d = vdict(d1)

        # get basic
        self.assertEqual(d.xget('key'),'1')

        # get default value
        self.assertEqual(d.xget('nokey','1'),'1')

        # get from sub dict
        self.assertEqual(d.xget('[SUB]key'),'X')

        # get from sub-sub dict
        self.assertEqual(d.xget('[SUB][SUB]key'),'Y')

        # Got None on not existat key
        self.assertIsNone(d.xget('nokey'))

        # And on not existant sub dict
        self.assertIsNone(d.xget('[NOSUB]nokey'))

        # Conditional keys

        # key does not exists if no define_set
        self.assertIsNone(d.xget('cond1'))

        # key value depends on define set value
        defines = DefineSet({ 'A': True })
        self.assertIsNone(d.xget('cond1'))
        self.assertEqual(d.xget('cond1',defines=defines),'A')
        self.assertIsNone(d.xget('cond1'))

        defines = DefineSet({ 'A': False })
        self.assertEqual(d.xget('cond1',defines=defines),'B')


# test purge
# test remove
# test select
# test select_as_list
# test select_null



def get_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(VDictTest))
    return suite

if __name__ == '__main__':
    utils.run_tests(get_suite())
  
