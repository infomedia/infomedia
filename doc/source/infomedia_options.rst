:mod:`infomedia.options` --- Options Class
==========================================

.. automodule:: infomedia.options


    .. autoclass:: Options()
       :members:
       :inherited-members: 

        .. automethod: get(label : str)

        .. automethod: select(regexps : list of str, regexp=False ) -> dict of options

